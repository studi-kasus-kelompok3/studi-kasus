drop DATABASE IF EXISTS studi_kasus;
CREATE DATABASE IF NOT EXISTS studi_kasus;
USE studi_kasus;

DROP TABLE IF EXISTS `accounts`;
DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` varchar(255) PRIMARY KEY,
  `email` varchar(255) UNIQUE,
  `address` varchar(255),
  `name` varchar(255)
);

CREATE TABLE `accounts` (
  `account_id` varchar(255) PRIMARY KEY,
  `msisdn_customer` varchar(255) UNIQUE,
  `user_id` varchar(255)
);

-- ALTER TABLE `users` ADD FOREIGN KEY (`account_id`) REFERENCES `accounts` (`account_id`);
ALTER TABLE `accounts` ADD FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

-- DELIMITER //
-- CREATE TRIGGER check_user_limit
-- BEFORE INSERT ON users
-- FOR EACH ROW
-- BEGIN
--     DECLARE user_count INT;
--     SELECT COUNT(*) INTO user_count FROM users WHERE account_id = NEW.account_id;
--     IF user_count >= 3 THEN
--         SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'User limit reached';
--     END IF;
-- END;
-- //
-- DELIMITER ;


DELIMITER //
CREATE TRIGGER check_account_limit
AFTER INSERT ON accounts
FOR EACH ROW
BEGIN
    DECLARE account_count INT;
    SELECT COUNT(*) INTO account_count FROM accounts WHERE user_id = NEW.user_id;
    IF account_count > 3 THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'User limit reached';
    END IF;
END;
//
DELIMITER ;
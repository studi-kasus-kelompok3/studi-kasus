package queries

import (
	"studi-kasus/bin/modules/user"
	"studi-kasus/bin/modules/user/models"
	"studi-kasus/bin/pkg/databases"
	"studi-kasus/bin/pkg/utils"

	"github.com/gin-gonic/gin"
)

type QueryRepository struct {
	ORM *databases.ORM
}

func NewQueryRepository(orm *databases.ORM) user.RepositoryQuery {
	return &QueryRepository{
		ORM: orm,
	}
}

func (q QueryRepository) FindOneByID(ctx *gin.Context, id string) utils.Result {
	var userModel models.User
	r := q.ORM.DB.First(&userModel, "user_id = ?", id)
	output := utils.Result{
		Data: userModel,
		DB:   r,
	}
	return output

}

func (q QueryRepository) FindOneByName(ctx *gin.Context, name string) utils.Result {
	var userModel models.User

	r := q.ORM.DB.First(&userModel, "name = ?", name)
	output := utils.Result{
		Data: userModel,
		DB:   r,
	}
	return output

}

package queries

import (
	"context"
	"fmt"
	"studi-kasus/bin/modules/user"
	"studi-kasus/bin/modules/user/models"
	"studi-kasus/bin/pkg/databases"
	"studi-kasus/bin/pkg/utils"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/gin-gonic/gin"
)

type MongoQueryRepository struct {
	Collection *mongo.Collection
}

func NewMongoQueryRepository(db *databases.MongoDatabase) user.MongoRepositoryQuery {
	return &MongoQueryRepository{
		Collection: db.GetCollection("users"),
	}
}

func (m MongoQueryRepository) FindOneByID(ctx *gin.Context, id string) utils.Result {
	var UserModel models.User
	err := m.Collection.FindOne(ctx, bson.M{"userid": id}).Decode(&UserModel)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return utils.Result{
				Error: fmt.Errorf("user not found"),
			}
		}
		return utils.Result{
			Error: err,
		}
	}

	return utils.Result{
		Data: UserModel,
	}

}

func (m MongoQueryRepository) FindOneByName(ctx *gin.Context, name string) utils.Result {
	var UserModel models.User
	err := m.Collection.FindOne(context.Background(), bson.M{"name": name}).Decode(&UserModel)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return utils.Result{
				Error: fmt.Errorf("user not found"),
			}
		}
		return utils.Result{
			Error: err,
		}
	}

	return utils.Result{
		Data: UserModel,
	}
}

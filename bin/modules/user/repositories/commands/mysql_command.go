package commands

import (
	"studi-kasus/bin/modules/user"
	"studi-kasus/bin/modules/user/models"
	"studi-kasus/bin/pkg/databases"
	"studi-kasus/bin/pkg/utils"

	"github.com/gin-gonic/gin"
)

type CommandRepository struct {
	ORM *databases.ORM
}

func NewCommandRepository(orm *databases.ORM) user.RepositoryCommand {
	return &CommandRepository{
		ORM: orm,
	}
}

func (c *CommandRepository) Create(ctx *gin.Context, u models.User) utils.Result {
	r := c.ORM.DB.Create(&u)
	output := utils.Result{
		Data: u,
		DB:   r,
	}
	return output
}

func (c *CommandRepository) Save(ctx *gin.Context, u models.User) utils.Result {
	r := c.ORM.DB.Save(&u)
	output := utils.Result{
		Data: u,
		DB:   r,
	}
	return output
}

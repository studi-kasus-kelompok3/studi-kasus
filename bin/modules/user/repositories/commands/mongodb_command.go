package commands

import (
	"context"
	"studi-kasus/bin/modules/user"
	"studi-kasus/bin/modules/user/models"
	"studi-kasus/bin/pkg/databases"
	"studi-kasus/bin/pkg/utils"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoCommandRepository struct {
	Collection *mongo.Collection
}

func NewMongoCommandRepository(db *databases.MongoDatabase) user.MongoRepositoryCommand {
	return &MongoCommandRepository{
		Collection: db.GetCollection("users"),
	}
}

func (c *MongoCommandRepository) Create(gctx *gin.Context, u models.User) utils.MongoInsertOneResult {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	r, err := c.Collection.InsertOne(ctx, u)
	if err != nil {
		return utils.MongoInsertOneResult{
			Error: err,
		}
	}

	output := utils.MongoInsertOneResult{
		Data:       u,
		Collection: r,
	}
	return output
}

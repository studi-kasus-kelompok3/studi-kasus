package usecases

import (
	"fmt"
	"net/http"
	"studi-kasus/bin/modules/user"
	"studi-kasus/bin/pkg/databases"

	"github.com/gin-gonic/gin"
)

type QueryUsecase struct {
	UserRepositoryQuery      user.RepositoryQuery
	UserMongoRepositoryQuery user.MongoRepositoryQuery
	ORM                      *databases.ORM
}

func NewQueryUsecase(q user.RepositoryQuery, m user.MongoRepositoryQuery, orm *databases.ORM) user.UsecaseQuery {
	return &QueryUsecase{
		UserRepositoryQuery:      q,
		UserMongoRepositoryQuery: m,
		ORM:                      orm,
	}
}

func (q QueryUsecase) GetByID(ctx *gin.Context) {
	id := ctx.Param("id")

	ret := q.UserRepositoryQuery.FindOneByID(ctx, id)
	if ret.DB.RowsAffected == 0 {
		ctx.AbortWithStatus(http.StatusFound)
		return
	}

	if ret.DB.Error != nil {
		ctx.AbortWithError(http.StatusBadRequest, ret.DB.Error)
		return
	}
	res := ret.Data
	ctx.JSON(http.StatusOK, res)
}

func (q QueryUsecase) GetByIDD(ctx *gin.Context) {
	id := ctx.Param("id")

	ret := q.UserMongoRepositoryQuery.FindOneByID(ctx, id)
	res := ret.Data

	ctx.JSON(http.StatusOK, res)
}

func (q QueryUsecase) GetAccess(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{"message": "user access success"})
}

func (q QueryUsecase) GetByName(ctx *gin.Context) {
	name := ctx.Param("name")
	fmt.Printf("name access %s", name)
	ret := q.UserRepositoryQuery.FindOneByName(ctx, name)
	ctx.JSON(http.StatusOK, ret.Data)
}

package usecases

import (
	"net/http"
	"strings"
	"studi-kasus/bin/modules/user"
	"studi-kasus/bin/modules/user/models"
	"studi-kasus/bin/pkg/databases"
	"studi-kasus/bin/pkg/utils/validators"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type CommandUsecase struct {
	UserRepositoryCommand      user.RepositoryCommand
	UserMongoRepositoryCommand user.MongoRepositoryCommand
	ORM                        *databases.ORM
}

func NewCommandUsecase(q user.RepositoryCommand, m user.MongoRepositoryCommand, orm *databases.ORM) user.UsecaseCommand {
	return &CommandUsecase{
		UserRepositoryCommand:      q,
		UserMongoRepositoryCommand: m,
		ORM:                        orm,
	}
}

func (q CommandUsecase) PostRegister(ctx *gin.Context) {
	var userModel models.User
	err := ctx.ShouldBind(&userModel)
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
	}

	// fmt.Println(userModel.Email)
	userModel.UserID = uuid.NewString()
	validEmail := validators.IsValidEmail(userModel.Email)
	// validUsername := validators.IsValidUsername(userModel.Username)
	// ValidPassword := validators.IsValidPassword(userModel.Password)

	if !validEmail {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "email not valid"})
		return
	}

	// if !validUsername {
	// 	ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "username not valid"})
	// 	return
	// }

	// if !ValidPassword {
	// 	ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "password not valid"})
	// 	return
	// }

	r := q.UserRepositoryCommand.Create(ctx, userModel)
	if r.DB.Error != nil {
		if strings.Contains(r.DB.Error.Error(), "duplicate key value violates unique constraint \"users_email_key\"") {
			ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "email or username already used"})
			return
		}

		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": r.DB.Error})
		return
	}
	userRegisterResponse := models.RegisterResponse{
		UserID:  userModel.UserID,
		Name:    userModel.Name,
		Address: userModel.Address,
		Email:   userModel.Email,
	}

	r = q.UserRepositoryCommand.Save(ctx, userModel)

	if r.DB.Error != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": r.DB.Error})
		return
	}

	mr := q.UserMongoRepositoryCommand.Create(ctx, userModel)
	if mr.Error != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": r.DB.Error})
		return
	}

	ctx.JSON(http.StatusOK, userRegisterResponse)
}

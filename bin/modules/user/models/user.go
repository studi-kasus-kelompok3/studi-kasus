package models

type User struct {
	UserID  string `gorm:"primaryKey" json:"userid"`
	Name    string `json:"name" form:"name"`
	Address string `json:"address" form:"address"`
	Email   string `json:"email" form:"email"`
}

type UpsertUser struct {
	Name    string `json:"name"`
	Address string `json:"address"`
	Email   string `json:"email"`
}

func (u User) UpsertUser() UpsertUser {
	return UpsertUser{
		Name:    u.Name,
		Address: u.Address,
		Email:   u.Email,
	}
}

type RegisterResponse struct {
	UserID  string `json:"userid"`
	Name    string `json:"name"`
	Address string `json:"address"`
	Email   string `json:"email"`
}

type GetUserResponse struct {
	UserID  string `json:"userid"`
	Name    string `json:"name"`
	Address string `json:"address"`
	Email   string `json:"email"`
}

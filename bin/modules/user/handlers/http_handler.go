package handlers

import (
	"studi-kasus/bin/modules/user"
	"studi-kasus/bin/pkg/servers"

	unitTest "github.com/Valiben/gin_unit_test"
)

type UserHttpHandler struct {
	UserUsecaseQuery   user.UsecaseQuery
	UserUsecaseCommand user.UsecaseCommand
}

func InitUserHTTPHandler(uq user.UsecaseQuery, uc user.UsecaseCommand, s *servers.GinServer) {
	handler := &UserHttpHandler{
		UserUsecaseQuery:   uq,
		UserUsecaseCommand: uc,
	}
	s.Gin.GET("/user/id/:id", handler.UserUsecaseQuery.GetByID)
	s.Gin.GET("/user/idd/:id", handler.UserUsecaseQuery.GetByIDD)
	s.Gin.GET("/user/", handler.UserUsecaseQuery.GetAccess)
	s.Gin.POST("/user/register", handler.UserUsecaseCommand.PostRegister)
	s.Gin.GET("/user/name/:name", handler.UserUsecaseQuery.GetByName)

	unitTest.SetRouter(s.Gin)
}

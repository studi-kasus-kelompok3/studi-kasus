package commands

import (
	"studi-kasus/bin/modules/account"
	"studi-kasus/bin/modules/account/models"
	"studi-kasus/bin/pkg/databases"
	"studi-kasus/bin/pkg/utils"

	"github.com/gin-gonic/gin"
)

type CommandRepository struct {
	ORM *databases.ORM
}

func NewCommandRepository(orm *databases.ORM) account.RepositoryCommand {
	return &CommandRepository{
		ORM: orm,
	}
}

func (c *CommandRepository) Create(ctx *gin.Context, a models.Account) utils.Result {
	r := c.ORM.DB.Create(&a)
	output := utils.Result{
		Data: a,
		DB:   r,
	}
	return output
}

func (c *CommandRepository) Save(ctx *gin.Context, a models.Account) utils.Result {
	r := c.ORM.DB.Save(&a)
	output := utils.Result{
		Data: a,
		DB:   r,
	}
	return output
}

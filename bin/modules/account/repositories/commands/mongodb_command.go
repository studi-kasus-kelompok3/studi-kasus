package commands

import (
	"context"
	"studi-kasus/bin/modules/account"
	"studi-kasus/bin/modules/account/models"
	"studi-kasus/bin/pkg/databases"
	"studi-kasus/bin/pkg/utils"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
)

type MongoCommandRepository struct {
	Collection *mongo.Collection
}

func NewMongoCommandRepository(db *databases.MongoDatabase) account.MongoRepositoryCommand {
	return &MongoCommandRepository{
		Collection: db.GetCollection("accounts"),
	}
}

func (c *MongoCommandRepository) Create(gctx *gin.Context, a models.Account) utils.MongoInsertOneResult {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	r, err := c.Collection.InsertOne(ctx, a)
	if err != nil {
		return utils.MongoInsertOneResult{
			Error: err,
		}
	}

	output := utils.MongoInsertOneResult{
		Data:       a,
		Collection: r,
	}
	return output
}

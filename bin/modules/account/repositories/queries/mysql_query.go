package queries

import (
	"studi-kasus/bin/modules/account"
	"studi-kasus/bin/modules/account/models"
	"studi-kasus/bin/pkg/databases"
	"studi-kasus/bin/pkg/utils"

	"github.com/gin-gonic/gin"
)

type QueryRepository struct {
	ORM *databases.ORM
}

func NewQueryRepository(orm *databases.ORM) account.RepositoryQuery {
	return &QueryRepository{
		ORM: orm,
	}
}

func (q QueryRepository) FindOneByID(ctx *gin.Context, id string) utils.Result {
	var accountModel models.Account
	r := q.ORM.DB.First(&accountModel, "account_id = ?", id)
	output := utils.Result{
		Data: accountModel,
		DB:   r,
	}
	return output

}

func (q QueryRepository) FindOneByName(ctx *gin.Context, name string) utils.Result {
	var accountModel models.Account

	r := q.ORM.DB.First(&accountModel, "name = ?", name)
	output := utils.Result{
		Data: accountModel,
		DB:   r,
	}
	return output

}

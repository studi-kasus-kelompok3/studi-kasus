package queries

import (
	"fmt"
	"studi-kasus/bin/modules/account"
	"studi-kasus/bin/modules/account/models"
	"studi-kasus/bin/pkg/databases"
	"studi-kasus/bin/pkg/utils"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/gin-gonic/gin"
)

type MongoQueryRepository struct {
	Collection *mongo.Collection
}

func NewMongoQueryRepository(db *databases.MongoDatabase) account.MongoRepositoryQuery {
	return &MongoQueryRepository{
		Collection: db.GetCollection("accounts"),
	}
}

func (m MongoQueryRepository) FindOneByID(ctx *gin.Context, id string) utils.Result {
	var accountModel models.Account
	err := m.Collection.FindOne(ctx, bson.M{"accountid": id}).Decode(&accountModel)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return utils.Result{
				Error: fmt.Errorf("account not found"),
			}
		}
		return utils.Result{
			Error: err,
		}
	}

	return utils.Result{
		Data: accountModel,
	}

}

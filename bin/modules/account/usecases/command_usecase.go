package usecases

import (
	"net/http"
	"strings"
	"studi-kasus/bin/modules/account"
	"studi-kasus/bin/modules/account/models"
	"studi-kasus/bin/pkg/databases"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type CommandUsecase struct {
	AccountRepositoryCommand      account.RepositoryCommand
	AccountMongoRepositoryCommand account.MongoRepositoryCommand
	ORM                           *databases.ORM
}

func NewCommandUsecase(q account.RepositoryCommand, m account.MongoRepositoryCommand, orm *databases.ORM) account.UsecaseCommand {
	return &CommandUsecase{
		AccountRepositoryCommand:      q,
		AccountMongoRepositoryCommand: m,
		ORM:                           orm,
	}
}

func (q CommandUsecase) PostRegister(ctx *gin.Context) {
	var accountModel models.Account
	err := ctx.ShouldBind(&accountModel)
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
	}

	accountModel.AccountID = uuid.NewString()

	// if !validAccountname {
	// 	ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "accountname not valid"})
	// 	return
	// }

	// if !ValidPassword {
	// 	ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "password not valid"})
	// 	return
	// }

	r := q.AccountRepositoryCommand.Create(ctx, accountModel)
	if r.DB.Error != nil {
		if strings.Contains(r.DB.Error.Error(), "duplicate key value violates unique constraint \"accounts_email_key\"") {
			ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "email or accountname already used"})
			return
		}

		if strings.Contains(r.DB.Error.Error(), "User limit reached") {
			ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": r.DB.Error.Error()})
			return
		}

		if strings.Contains(r.DB.Error.Error(), "Cannot add or update a child row: a foreign key constraint fails (`studi_kasus`.`accounts`, CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`))") {
			ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": "a foreign key constraint fails, can not find userid"})
			return
		}

		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "InternalServerError"})
		return
	}

	accountRegisterResponse := models.RegisterResponse{
		AccountID:      accountModel.AccountID,
		MsisdnCustomer: accountModel.MsisdnCustomer,
		UserID:         accountModel.UserID,
	}

	r = q.AccountRepositoryCommand.Save(ctx, accountModel)

	if r.DB.Error != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "InternalServerError"})
		return
	}

	mr := q.AccountMongoRepositoryCommand.Create(ctx, accountModel)
	if mr.Error != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": "InternalServerError"})
		return
	}

	ctx.JSON(http.StatusOK, accountRegisterResponse)
}

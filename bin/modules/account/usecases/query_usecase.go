package usecases

import (
	"net/http"
	"studi-kasus/bin/modules/account"
	"studi-kasus/bin/pkg/databases"

	"github.com/gin-gonic/gin"
)

type QueryUsecase struct {
	AccountRepositoryQuery      account.RepositoryQuery
	AccountMongoRepositoryQuery account.MongoRepositoryQuery

	ORM *databases.ORM
}

func NewQueryUsecase(q account.RepositoryQuery, m account.MongoRepositoryQuery, orm *databases.ORM) account.UsecaseQuery {
	return &QueryUsecase{
		AccountRepositoryQuery:      q,
		AccountMongoRepositoryQuery: m,
		ORM:                         orm,
	}
}

func (q QueryUsecase) GetByID(ctx *gin.Context) {
	id := ctx.Param("id")

	ret := q.AccountRepositoryQuery.FindOneByID(ctx, id)
	if ret.DB.RowsAffected == 0 {
		ctx.AbortWithStatus(http.StatusFound)
		return
	}

	if ret.DB.Error != nil {
		ctx.AbortWithError(http.StatusBadRequest, ret.DB.Error)
		return
	}
	res := ret.Data
	ctx.JSON(http.StatusOK, res)
}

func (q QueryUsecase) GetByIDD(ctx *gin.Context) {
	id := ctx.Param("idd")

	ret := q.AccountMongoRepositoryQuery.FindOneByID(ctx, id)
	res := ret.Data

	ctx.JSON(http.StatusOK, res)
}

func (q QueryUsecase) GetAccess(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{"message": "account access success"})
}

package handlers

import (
	"studi-kasus/bin/modules/account"
	"studi-kasus/bin/pkg/servers"
)

type AccountHttpHandler struct {
	AccountUsecaseQuery   account.UsecaseQuery
	AccountUsecaseCommand account.UsecaseCommand
}

func InitAccountHTTPHandler(uq account.UsecaseQuery, uc account.UsecaseCommand, s *servers.GinServer) {
	handler := &AccountHttpHandler{
		AccountUsecaseQuery:   uq,
		AccountUsecaseCommand: uc,
	}
	s.Gin.GET("/account/id/:id", handler.AccountUsecaseQuery.GetByID)
	s.Gin.GET("/account/", handler.AccountUsecaseQuery.GetAccess)
	s.Gin.POST("/account/register", handler.AccountUsecaseCommand.PostRegister)
	s.Gin.GET("/account/idd/:idd", handler.AccountUsecaseQuery.GetByIDD)
}

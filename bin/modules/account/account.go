package account

import (
	"studi-kasus/bin/modules/account/models"
	"studi-kasus/bin/pkg/utils"

	"github.com/gin-gonic/gin"
)

type UsecaseQuery interface {
	GetByID(ctx *gin.Context)
	GetAccess(ctx *gin.Context)
	GetByIDD(ctx *gin.Context)
}

type UsecaseCommand interface {
	PostRegister(ctx *gin.Context)
}

type RepositoryQuery interface {
	FindOneByID(ctx *gin.Context, id string) utils.Result
}

type RepositoryCommand interface {
	Create(ctx *gin.Context, u models.Account) utils.Result
	Save(ctx *gin.Context, u models.Account) utils.Result
}

type MongoRepositoryQuery interface {
	FindOneByID(ctx *gin.Context, id string) utils.Result
}

type MongoRepositoryCommand interface {
	Create(ctx *gin.Context, u models.Account) utils.MongoInsertOneResult
}

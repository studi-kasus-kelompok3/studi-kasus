package models

type Account struct {
	AccountID      string `gorm:"primaryKey" json:"accountid"`
	MsisdnCustomer string `json:"msisdncustomer" form:"msisdncustomer"`
	UserID         string `json:"userid" form:"userid"`
	// User           models.User `gorm:"foreignKey:userid" json:"userid"`
}

type UpsertAccount struct {
	MsisdnCustomer string `json:"msisdncustomer"`
	UserID         string `json:"userid"`
}

func (a Account) UpsertUser() UpsertAccount {
	return UpsertAccount{
		MsisdnCustomer: a.MsisdnCustomer,
		UserID:         a.UserID,
	}
}

type RegisterResponse struct {
	AccountID      string `json:"accountid"`
	MsisdnCustomer string `json:"msisdncustomer"`
	UserID         string `form:"userid"`
}

type GetAccountResponse struct {
	UserID  string `json:"userid"`
	Name    string `json:"name"`
	Address string `json:"address"`
	Email   string `json:"email"`
}

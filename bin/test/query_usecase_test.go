package test

import (
	accountModel "studi-kasus/bin/modules/account/models"
	userModel "studi-kasus/bin/modules/user/models"
	"testing"

	unitTest "github.com/Valiben/gin_unit_test"
	"github.com/Valiben/gin_unit_test/utils"
)

// Our test will see if we receive an array of samples
func TestGetByUserID(t *testing.T) {
	var users userModel.User

	expected := "7dcd1668-8dcf-4bd3-8388-397826b59f02"

	err := unitTest.TestHandlerUnMarshalResp(utils.GET, "/user/id/7dcd1668-8dcf-4bd3-8388-397826b59f02", "json", nil, &users)

	if err != nil {
		t.Errorf("TestGetByUserID: %v\n", err)
		return
	}

	if users.UserID != expected {
		t.Errorf("TestGetByUserID: Expected %v but got %v", expected, users.Email)
		return
	}

	t.Log("TestGetByUserID Success")
}

func TestGetByUserIDD(t *testing.T) {
	var users userModel.User

	expected := "7dcd1668-8dcf-4bd3-8388-397826b59f02"

	err := unitTest.TestHandlerUnMarshalResp(utils.GET, "/user/idd/7dcd1668-8dcf-4bd3-8388-397826b59f02", "json", nil, &users)

	if err != nil {
		t.Errorf("TestGetByUserIDD: %v\n", err)
		return
	}

	if users.UserID != expected {
		t.Errorf("TestGetByUserIDD: Expected %v but got %v", expected, users.Email)
		return
	}

	t.Log("TestGetByUserIDD Success")
}

func TestGetByAccountID(t *testing.T) {
	var account accountModel.Account

	expected := "9964f5ff-9fdd-4ff6-a436-e8e6b93acbe2"

	err := unitTest.TestHandlerUnMarshalResp(utils.GET, "/account/id/9964f5ff-9fdd-4ff6-a436-e8e6b93acbe2", "json", nil, &account)

	if err != nil {
		t.Errorf("TestGetByAccountID: %v\n", err)
		return
	}

	if account.AccountID != expected {
		t.Errorf("TestGetByAccountID: Expected %v but got %v", expected, account.AccountID)
		return
	}

	t.Log("TestGetByAccountID Success")
}

func TestGetByAccountIDD(t *testing.T) {
	var account accountModel.Account

	expected := "9964f5ff-9fdd-4ff6-a436-e8e6b93acbe2"

	err := unitTest.TestHandlerUnMarshalResp(utils.GET, "/account/idd/9964f5ff-9fdd-4ff6-a436-e8e6b93acbe2", "json", nil, &account)

	if err != nil {
		t.Errorf("TestGetByAccountIDD: %v\n", err)
		return
	}

	if account.AccountID != expected {
		t.Errorf("TestGetByAccountIDD: Expected %v but got %v", expected, account.AccountID)
		return
	}

	t.Log("TestGetByAccountIDD Success")
}

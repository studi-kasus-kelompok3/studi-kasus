package main

import (
	"log"
	"os"
	userHandler "studi-kasus/bin/modules/user/handlers"
	userRepositoryCommands "studi-kasus/bin/modules/user/repositories/commands"
	userRepositoryQueries "studi-kasus/bin/modules/user/repositories/queries"
	userUsecases "studi-kasus/bin/modules/user/usecases"

	accountHandler "studi-kasus/bin/modules/account/handlers"
	accountRepositoryCommands "studi-kasus/bin/modules/account/repositories/commands"
	accountRepositoryQueries "studi-kasus/bin/modules/account/repositories/queries"
	accountUsecases "studi-kasus/bin/modules/account/usecases"

	"studi-kasus/bin/pkg/databases"
	"studi-kasus/bin/pkg/servers"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load() // Load ENV

	if err != nil {
		log.Fatal(err)
	}

	srv := &servers.GinServer{}                 // Make server instance
	orm := &databases.ORM{}                     // Make instance database with ORM
	mongoDatabase := &databases.MongoDatabase{} // Make mongo instance
	// userHandler := &handlers.UserServer{}

	// Load ENV
	dsn := os.Getenv("DB_DSN")
	mongoUri := os.Getenv("MONGODB_URI")

	srv.InitGin()
	orm.InitDB(dsn)
	mongoDatabase.InitDB(mongoUri)
	// srv.InitTryRoutes()
	setHTTP(orm, mongoDatabase, srv)
	srv.Start(":8050", orm, mongoDatabase)
}

func setHTTP(orm *databases.ORM, mongodb *databases.MongoDatabase, srv *servers.GinServer) {
	userQueryRepository := userRepositoryQueries.NewQueryRepository(orm)
	userMongoQueryRepository := userRepositoryQueries.NewMongoQueryRepository(mongodb)
	userQueryUsecase := userUsecases.NewQueryUsecase(userQueryRepository, userMongoQueryRepository, orm)
	userCommandRepository := userRepositoryCommands.NewCommandRepository(orm)
	userMongoCommandRepository := userRepositoryCommands.NewMongoCommandRepository(mongodb)
	userCommandUsecase := userUsecases.NewCommandUsecase(userCommandRepository, userMongoCommandRepository, orm)
	userHandler.InitUserHTTPHandler(userQueryUsecase, userCommandUsecase, srv)

	accountQueryRepository := accountRepositoryQueries.NewQueryRepository(orm)
	accountMongoQueryRepository := accountRepositoryQueries.NewMongoQueryRepository(mongodb)
	accountQueryUsecase := accountUsecases.NewQueryUsecase(accountQueryRepository, accountMongoQueryRepository, orm)
	accountCommandRepository := accountRepositoryCommands.NewCommandRepository(orm)
	accountMongoCommandRepository := accountRepositoryCommands.NewMongoCommandRepository(mongodb)
	accountCommandUsecase := accountUsecases.NewCommandUsecase(accountCommandRepository, accountMongoCommandRepository, orm)
	accountHandler.InitAccountHTTPHandler(accountQueryUsecase, accountCommandUsecase, srv)
}

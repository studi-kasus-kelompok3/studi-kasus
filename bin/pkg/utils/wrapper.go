package utils

import (
	"studi-kasus/bin/modules/user/models"

	"go.mongodb.org/mongo-driver/mongo"
	"gorm.io/gorm"
)

type Result struct {
	Data  interface{}
	DB    *gorm.DB
	Error error
}

type MongoInsertOneResult struct {
	Data       interface{}
	Collection *mongo.InsertOneResult
	Error      error
}

type FindPasswordResult struct {
	Data     models.User
	Password string
	DB       *gorm.DB
	Error    error
}

package validators

import (
	"regexp"
)

func IsValidEmail(email string) bool {
	// Email validation regular expression
	emailRegex := `^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$`

	// Compile the regular expression
	regex := regexp.MustCompile(emailRegex)

	// Check if the email matches the regular expression
	return regex.MatchString(email)
}

func IsValidPassword(password string) bool {
	// Password validation regular expression
	passwordRegex := `^(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9]).{6,10}$`

	// Compile the regular expression
	regex := regexp.MustCompile(passwordRegex)

	// Check if the password matches the regular expression
	return regex.MatchString(password)
}

func IsValidUsername(username string) bool {
	// Username validation regular expression
	usernameRegex := `^[^\s]+$`

	// Compile the regular expression
	regex := regexp.MustCompile(usernameRegex)

	// Check if the username matches the regular expression
	return regex.MatchString(username)
}

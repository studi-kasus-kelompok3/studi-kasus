package databases

import (
	"log"

	"studi-kasus/bin/modules/user/models"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type ORM struct {
	DB *gorm.DB
}

func (o *ORM) InitDB(dsn string) *ORM {
	// db, err := gorm.Open(postgres.Open(dsn)) // make db instance from postgres dsn
	db, err := gorm.Open(mysql.Open(dsn)) // make db instance from postgres dsn

	db.AutoMigrate(&models.User{})

	if err != nil {
		log.Fatal(err)
	}

	o.DB = db
	return o
}

// check if database orm ready
func (o *ORM) Ready() bool {
	return o.DB != nil
}

package databases

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoDatabase struct {
	DB *mongo.Client
}

func (m *MongoDatabase) InitDB(uri string) {
	client, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		log.Fatal(err)
	}

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}

	//ping the database
	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connected to MongoDB")
	m.DB = client

}

func (o *MongoDatabase) Ready() bool {
	return o.DB != nil
}

func (o *MongoDatabase) GetCollection(collectionName string) *mongo.Collection {
	collection := o.DB.Database(os.Getenv("MONGODB_DB")).Collection(collectionName)
	return collection
}
